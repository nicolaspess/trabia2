#coding: utf-8

from .State import State
from random import randint
import numpy
import math
import random
import datetime, time
import operator
import os
import glob
import string

class Controller:
	tabelaQ = {} #tabelaQ é um dicionario
	epson = 0.01
	iteracoes = 1
	# Quanto menor o valor do aprendizado mais rapido o epson vai crescer
	# 100 ~ deixa de ser aleatorio em poucos segundos
	# 1000 ~ deixa de ser aleatorio em poucos minutos
	# 10000 ~ deixa de ser aleatorio em mais tempo
	aprendizado = 1000

	def __init__(self, load, state):
		self.state = state
		self.init_table_Q(load, state)


	# Imprime a tabela
	def print_table(self):
		for (j, x) in enumerate(self.tabelaQ):
			print str(j) + str(self.tabelaQ[x])


    # TODO: carrega a tabela Q de um arquivo (se load!=None, entao load ira conter o nome do arquivo a ser carregado),
    # ou, caso load==None, a funcao de inicializar uma tabela Q manualmente.
    # Dica: a tabela Q possui um valor para cada possivel par de estado e acao. Cada objeto do tipo State possui um id unico
    # (calculado por State.get_state_id), o qual pode ser usado para indexar a sua tabela Q, juntamente com o indice da acao.
    # Para criacao da tabela Q, pode ser importante saber o numero total de estados do sistema. Isso dependera de quantas features
    # voce utilizar e em quantos niveis ira discretiza-las (ver arquivo State.py para mais detalhes). O numero total de
    # estados do sistema pode ser obtido atraves do metodo State.get_n_states.
    # Uma lista completa com os estados propriamente ditos pode ser obtida atraves do metodo State.states_list.
	def init_table_Q(self,load, state):
		#pass
		if load != None:
			#ler arquivo para inicializar Q

			newest = max(glob.iglob('./params/*.txt'), key=os.path.getctime) #pega arquivo mais novos
			f = open(newest, 'r')
			texto = f.readlines()
			count = 1
			for linha in texto:
				if count != 1:
					l = linha.split(';')
					l = map(float, l) #passa de string pra int
					self.tabelaQ[int(l[0])] = l[1:5] # valores de cada ação para determinado estado l[1]->direita, l[2]->esquerda, l[3]->acaoNula, l[4]->atirar
				else:
					self.epson = float(linha)
					count = 0
		else:
			#inicializar manualmente
			for estado in state.states_list():
				self.tabelaQ[state.get_state_id(estado)] = [0, 0, 0, 0]
				#no estado N as acoes seguem assim: direita = 0, esquerda=0, acaoNula=0, atirar=0

	# TODO: salvar a tabela Q aprendida para um arquivo--para posteriormente poder ser lida por init_table_Q
	def save_table_Q(self, episode, state):
		#Escrever nesse arquivo que esta sendo criado na pasta params
		if episode > 0 and episode % 10 == 0:
			print "Salvando tabela no arquivo " + str(datetime.datetime.fromtimestamp(time.time()).strftime('%Y%m%d%H%M%S')) + ".txt"
			if not os.path.exists("./params"):
				os.makedirs("./params")
			output = open("./params/%s.txt" % datetime.datetime.fromtimestamp(time.time()).strftime('%Y%m%d%H%M%S'), "w+")
			#salva epson
			output.write(str(self.epson)+"\n")
			#percorre a tabelaQ -> é um dicionario
			for key, value in self.tabelaQ.iteritems():
				output.write(str(key) + ';' + str(value[0]) + ';' + str(value[1]) + ';' + str(value[2]) + ';' + str(value[3]) + "\n")
			output.close()


	# TODO: funcao que calcula recompensa a cada passo
	# Recebe como o parametro a acao executada, o estado anterior e posterior a execucao dessa acao,
	# o numero de passos desde o inicio do episodio, e um booleano indicando se o episodio acabou apos a execucao da acao.
	# Caso o episodio tenha terminado, o ultimo parametro especifica como ele terminou (IA "won", IA "lost", "draw" ou "collision")
    # Todas essas informacoes podem ser usadas para determinar que recompensa voce quer dar para o agente nessa situacao
	def compute_reward(self, action, prev_state, curr_state, nsteps, isEpisodeOver, howEpisodeEnded):

		# FUNCAO F1
		if howEpisodeEnded == "won":
			return 100
		if howEpisodeEnded == "lost":
			return -100
		if howEpisodeEnded == "draw":
			return -10
		if howEpisodeEnded == "collision":
			return -50
		return -0.1
		
		# FUNCAO F2
# 		if howEpisodeEnded == "won":
# 			return 1
# 		if howEpisodeEnded == "lost":
# 			return -10
# 		if howEpisodeEnded == "draw":
# 			return -10
# 		if howEpisodeEnded == "collision":
# 			return -10
# 		return -0.001		


	# Exploracao de boltzmann
	# Recebe as acoes com seus pesos e a temperatura T e retorna o indice da acao que sera executada
	def boltzmann(self, acoes, temp):

		probabilidades = []
		lista_indices = []
		divisor = 0

		for (i, y) in enumerate(acoes):
			lista_indices.append(i)
			divisor = divisor + math.e ** (acoes[i] / temp)

		for (j, x) in enumerate(acoes):
			probabilidades.append((math.e ** (acoes[j] / temp)) / divisor)

		arr = numpy.random.choice(len(lista_indices), 1, probabilidades)
		return arr[0]


	# Exploracao greedy
	# escolhe a acao de maior peso com epson chance
	# escolhe acao aleatoria com 1-epson chance
	def greedy(self, acoes, epson):
		if (random.random() < epson):
 			return numpy.argmax(acoes)
		else:
 			return random.randint(0,len(acoes)-1)


	# ESTA TAKE_ACTION DEVE SER USADA NA BATALHA (EVALUATE)
	# DESCOMENTAR E COMENTAR A OUTRA
# 	def take_action(self, state):
# 		s = state.get_state_id(state.get_state())
# 		acao = numpy.argmax(self.tabelaQ[s])
# 		return acao + 1


	# ESTA TAKE_ACTION DEVE SER USADA NO MODO LEARN
	# TODO: Deve consultar a tabela Q e escolher uma acao de acordo com a politica de exploracao
	# Retorna 1 caso a acao desejada seja direita, 2 caso seja esquerda, 3 caso seja nula, e 4 caso seja atirar
	def take_action(self, state):

		# Guarda o numero de iteracoes
		self.iteracoes = self.iteracoes + 1

		# A cada tantas iteracoes aumenta a chance de escolher o maior peso de acao
		if (self.iteracoes % self.aprendizado == 0):
			self.iteracoes = 1
			if (self.epson < 1.0):
				self.epson = self.epson + 0.01
				print "Epson atualizado para " + str(self.epson)

		s = state.get_state_id(state.get_state())
		acao = self.greedy(self.tabelaQ[s], self.epson)

		# acao sera um valor entre 0 e 3
		# somamos 1 para ficar entre 1 e 4
		return acao + 1


	# TODO: Implementa a regra de atualziacao do Q-Learning.
	# Recebe como o parametro a acao executada, o estado anterior e posterior a execucao dessa acao,
	# a recompensa obtida e um booleano indicando se o episodio acabou apos a execucao da acao
	def updateQ(self, action, prev_state, curr_state, reward, isEpisodeOver):
		alpha = 0.5
		gama = 1
		idPrevState = prev_state.get_state_id(prev_state.get_state())
		idCurrState = curr_state.get_state_id(curr_state.get_state())

		# action sera um valor entre 1 e 4
		# subtraimos 1 para ficar entre 0 e 3 p/ nao gerar keyerror
		action = action - 1

# 		print "----------------"
# 		print idPrevState
# 		print idCurrState
# 		print action
# 		print self.tabelaQ[idPrevState]
# 		print "----------------"

		self.tabelaQ[idPrevState][action] = (1-alpha) * self.tabelaQ[idPrevState][action] + alpha * (reward + gama * numpy.amax(self.tabelaQ[idCurrState]))
		return
