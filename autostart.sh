#!/bin/sh

ulimit -Sc unlimited

# Caminho para binario do python
PYTHON_BIN="/usr/local/Cellar/python/2.7.11/bin/python2.7"

COMANDO=$0
MODO=$1
BOT=$2
PESO=$3

# Verificando se recebeu os parâmetros obrigatórios
if [ -z $COMANDO ] || [ -z $MODO ] || [ -z $BOT ] ; then
	echo -e "Modo de uso: $COMANDO learn|evaluate lazy_bot|ninja_bot [nome do arquivo]"
	exit 0
fi

while [ 1 ] ; do
	echo restarting at time at `date +"%Y-%m-%d-%H:%M:%S"` >> startlog.txt
	$PYTHON_BIN ./__init__.py $MODO $BOT $PESO
	echo "Reiniciando bot..."
	sleep 5
done
